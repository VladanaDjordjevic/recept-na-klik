import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { User } from '../services/user/user.model';
import { UserService } from '../services/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';
import { RecipeService } from '../services/recipe/recipe.service';
import { RecipeModel } from '../services/recipe/recipe.model';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { DomSanitizer } from '@angular/platform-browser';
import { join } from 'path';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public user: User;
  public recipeUpdate : RecipeModel;
  public userRecipes: RecipeModel[];
  public userFavorites: RecipeModel[] = [];
  public UpdateForm: FormGroup;
  public UpdateFormRecipe : FormGroup;
  public fileToUpload: File;
  public fileToUpload1: File;
  public imageContent;
  public allRecipes: RecipeModel[];
  equalpassword: boolean = false;
  public isUserSame: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute,
    public loginService: LoginService,
    private domSanitizer: DomSanitizer,
    private cdr : ChangeDetectorRef) {
    this.findUserById();
    this.UpdateForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirm: [''],
      photo:[]
    });
    this.UpdateFormRecipe = this.formBuilder.group({
      title : [],
      difficulty : [],
      preptime : [],
      category : this.formBuilder.array([]),
      ingredients : this.formBuilder.array([]),
      instructions : [],
      course : [],
      units : [],
      serving : [],
      photo : []
    });
  }

  ngOnInit(): void {
  }

  openEditRecipeDiv: boolean = false;

  public openEditRecipe(id) {
    this.openEditRecipeDiv = true;
    this.recipeService.getRecipeById(id)
        .subscribe((recipe: RecipeModel) => {
          this.recipeUpdate = recipe;
          const category: FormArray = this.UpdateFormRecipe.get('category') as FormArray;
          recipe.category.forEach((item) => {
            category.push(new FormControl(item));
            (document.getElementById(item) as HTMLInputElement).checked = true;
          });

          const ingrs = this.UpdateFormRecipe.controls.ingredients as FormArray;
          recipe.ingredients.forEach((item) => {
            var c = this.formBuilder.control({});
            c.setValue(item);
            ingrs.push(c);
          });

          (this.UpdateFormRecipe.controls.instructions as FormControl).setValue(recipe.instructions.join("\r\n"));

          this.recipeService.refreshRecipes("");
          this.userService.getRecipesForUser(this.user._id).subscribe((recipes) => this.userRecipes = recipes);
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        });

        document.getElementById("aboutUser").style.visibility = "hidden" ;
  }

  public closeEditRecipe() {
    this.openEditRecipeDiv = false;
    this.UpdateFormRecipe.reset();
    const ingrs = this.UpdateFormRecipe.controls.ingredients as FormArray;
      while (ingrs.length !== 0) {
        ingrs.removeAt(0);
      }

    const catsControl = this.UpdateFormRecipe.controls.category as FormArray;
    while (catsControl.length !== 0) {
      catsControl.removeAt(0);
    }
    this.recipeService.refreshRecipes("");
    this.userService.getRecipesForUser(this.user._id).subscribe((recipes) => this.userRecipes = recipes);

    this.cdr.markForCheck();
    this.cdr.detectChanges();

    document.getElementById("aboutUser").style.visibility = "visible" ;
  }

  openedit: boolean = false;
  openeditdiv() {
    this.openedit = true;
    document.getElementById("aboutUser").style.visibility = "hidden";

  }
  closeeditdiv() {
    this.openedit = false;
    document.getElementById("aboutUser").style.visibility = "visible" ;
    this.userService.getUserById(this.user._id).subscribe((user) => {
      this.user = user;
      let TYPED_ARRAY = new Uint8Array(user.photo['data']['data']);
      const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
      let base64String = btoa(STRING_CHAR);
      this.imageContent = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);
    });
  }
  public submitForm(data) {
    if (!this.UpdateForm.valid) {
      window.alert('Not valid!');
      return;
    }
console.log('usao prvu');
    this.userService.updateUser(this.toFormData1(this.UpdateForm.value),this.user._id)
      .subscribe((data) => {
        window.alert('Izmene su sačuvane!');
        this.UpdateForm.reset();
        this.closeeditdiv();
      });
  }

  public submitFormRecipe(data) {
    if (!this.UpdateFormRecipe.valid){
      window.alert("Forma nije validna!");
      return;
    }

    this.recipeService.updateRecipe(this.toFormData(this.UpdateFormRecipe.value), this.recipeUpdate._id
    ).subscribe((data) => {
      window.alert("Izmene su sačuvane!");
      console.log("posle update");
      this.UpdateFormRecipe.reset();
      this.closeEditRecipe();
    });

  }

  public toFormData1 (formValue) {
    const formData = new FormData();

    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      if (value != '' && value != null){
        formData.append(key, value);
      }
    }
    formData.delete('confirm');

    return formData;
  }
  public toFormData (formValue) {
    const formData = new FormData();

    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      if (value != '' && value != null){
        formData.append(key, value);
      }
    }
    console.log('usao funkciju');
    return formData;
  }

  public addIngredient() {
    const ingrs = this.UpdateFormRecipe.controls.ingredients as FormArray;
    var c = this.formBuilder.control({});
    c.setValue('');
    ingrs.push(c);
  }

  public customTrackBy(index: number, obj: any): any {
    return index;
  }

  public onCheckboxChange(e) {
    const category: FormArray = this.UpdateFormRecipe.get('category') as FormArray;

    if (e.target.checked) {
      category.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      category.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          category.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


  private findUserById() {
    const userSub = this.route.paramMap.pipe(
      map((params) => params.get('userId')),
      switchMap((userId) => this.userService.getUserById(userId))
    ).subscribe((user) => {
      this.user = user;
      this.user.favorites.splice(this.user.favorites.length - 1);

      this.user.favorites.forEach(x => {
        this.recipeService.getRecipeById(x).subscribe(recipe => this.userFavorites.push(recipe));
      });

      let TYPED_ARRAY = new Uint8Array(user.photo['data']['data']);
      const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
      let base64String = btoa(STRING_CHAR);
      this.imageContent = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);

      
      if (this.loginService.getLoggedIn() && this.loginService.getLoggedInUserId() == user._id) {
        this.isUserSame = true;
      } else {
        this.isUserSame = false;
      }
    });

    const recipes = this.route.paramMap.pipe(
      map((params) => params.get('userId')),
      switchMap((userId) => this.userService.getRecipesForUser(userId))
    ).subscribe((recipes) => this.userRecipes = recipes);

    const allRecipes = this.recipeService.refreshRecipes("").subscribe(recipes => this.allRecipes = recipes);
  }

  public deleteRecipe(id: string) {
    const deleteSub = this.recipeService.deleteRecipe(id).subscribe(() => {
      window.alert('Vaš recept je obrisan!');
      this.userService.getRecipesForUser(this.user._id).subscribe((result) => this.userRecipes = result);
    });
  }

  public get email() {
    return this.UpdateForm.get('email');
  }
  public get username() {
    return this.UpdateForm.get('username');
  }
  public get password() {
    return this.UpdateForm.get('password');
  }

  public get firstName() {
    return this.UpdateForm.get('firstName');
  }
  public get oldpassword() {
    return this.UpdateForm.get('oldpassword');
  }

  public get lastName() {
    return this.UpdateForm.get('lastName');
  }
  public get confirm() {
    return this.UpdateForm.get('confirm');
  }

  fileChangeEvent(fileInput: any) {
    const fileToUpload1 = <File>fileInput.target.files[0];
    //const formData = new FormData();
    //formData.append("photo1",fileToUpload1);
    //this.UpdateForm.get('photo1').setValue(fileToUpload1);
      this.UpdateForm.get('photo').setValue(fileToUpload1);
  }

  fileChangeEventRecipe(fileInput: any) {
    const fileToUpload = <File>fileInput.target.files[0];
    this.UpdateFormRecipe.get('photo').setValue(fileToUpload)
  }

  findUserIdForFavorite(id: string) {
    if (this.allRecipes) {
      return this.allRecipes.forEach(x => {
        if (x._id == id) {
          return x.userId;
        }
      })
    }
  }

  deleteFavorite(recipeId: string) {
    this.userService.deleteFavorite(this.user._id, recipeId).subscribe(() => {
      for (let i = 0; i < this.userFavorites.length; i++) {
        if (this.userFavorites[i]._id == recipeId) {
          this.userFavorites.splice(i, 1);
          break;
        }
      }
      window.alert('Recept je obrisan iz liste omiljenih recepata!');
    });
  }
  public equalpass(){
    if(this.password.value==this.confirm.value)
       this.equalpassword=false;
       else
       this.equalpassword=true;
   return this.equalpassword;
   }
}
