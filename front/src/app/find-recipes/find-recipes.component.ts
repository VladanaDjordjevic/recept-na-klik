import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { RecipeService } from '../services/recipe/recipe.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { RecipeModel } from '../services/recipe/recipe.model';

@Component({
  selector: 'app-find-recipes',
  templateUrl: './find-recipes.component.html',
  styleUrls: ['./find-recipes.component.css']
})
export class FindRecipesComponent implements OnInit {

  public recipes : RecipeModel[] = [];
  public checkoutForm : FormGroup;
  constructor(private formBuilder: FormBuilder, private recipeService: RecipeService) { 
    this.checkoutForm = this.formBuilder.group({
      minDifficulty : [],
      maxDifficulty : [],
      minPreptime : [],
      maxPreptime : [],
      category : this.formBuilder.array([]),
      ingredients : this.formBuilder.array([]),
      score : []
    });
    this.recipeService.getRecipes().subscribe((recipes : RecipeModel[]) => {
      this.recipes = recipes;
    });
  }

  ngOnInit(): void {
  }

  public addIngredient() {
    const ingrs = this.checkoutForm.controls.ingredients as FormArray;
    var c = this.formBuilder.control({});
    c.setValue('');
    ingrs.push(c);
  }

  public customTrackBy(index: number, obj: any): any {
    return index;
  }

  public onCheckboxChange(e) {
    const category: FormArray = this.checkoutForm.get('category') as FormArray;
  
    if (e.target.checked) {
      category.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      category.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          category.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


  public submitForm(data){
    if (!this.checkoutForm.valid){
      window.alert("Forma nije validna!");
      return;
    }

    const category = this.checkoutForm.get('category') as FormArray;
    const ingrs = this.checkoutForm.controls.ingredients as FormArray;
    const minDiff = this.checkoutForm.controls.minDifficulty as FormControl;
    const maxDiff = this.checkoutForm.controls.maxDifficulty as FormControl;
    const minPrepT = this.checkoutForm.controls.minPreptime as FormControl;
    const maxPrepT = this.checkoutForm.controls.maxPreptime as FormControl;
    const sc = this.checkoutForm.controls.score as FormControl;

    if (category.length == 0 && ingrs.length == 0 && minDiff.value == '' && maxDiff.value == '' && minPrepT.value == '' && maxPrepT.value == '' && sc.value==''){
      window.alert("Forma nije validna, niste uneli ni jedan uslov filtriranja!");
    }

    var filterString = "?";
    var forConcat = 0;
    if (sc.value != null){
      filterString = filterString + "score=" + sc.value;
      forConcat = 1;
    }

    var maxDiffExists = 0;
    if (maxDiff.value != null && maxDiff.value != ''){
      maxDiffExists = 1;
      if (forConcat){
        filterString += "&";
      }
      filterString = filterString +"difficulty={\"lt\":" + maxDiff.value;
      forConcat = 1;
    }

    if (minDiff.value != null && minDiff.value != ''){
      if (maxDiffExists){
        filterString += ",\"gt\":" + minDiff.value;
      }
      else {
        if (forConcat){
          filterString += "&";
        }
        filterString = filterString +"difficulty={\"gt\":" + minDiff.value + "}";
      }
      forConcat = 1;
    }
    if (maxDiffExists){
      filterString += "}";
    }

    var maxPrepExists = 0;
    if (maxPrepT.value != null && maxPrepT.value != ''){
      maxPrepExists = 1;
      if (forConcat){
        filterString += "&";
      }
      filterString = filterString +"preptime={\"lt\":" + maxPrepT.value;
      forConcat = 1;
    }

    if (minPrepT.value != null && minPrepT.value != ''){
      if (maxPrepExists){
        filterString += ",\"gt\":" + minPrepT.value;
      }
      else {
        if (forConcat){
          filterString += "&";
        }
        filterString = filterString +"preptime={\"gt\":" + minPrepT.value + "}";
      }
      forConcat = 1;
    }
    if (maxPrepExists){
      filterString += "}";
    }

    if (category.length != 0){
      for (var i = 0; i < category.length; i++) { 
        if (forConcat){
          filterString += "&";
        }
        filterString += "category[]=" + category.at(i).value;
        if (i != category.length-1){
          filterString += "&";
        }
      }
      forConcat = 1;
    }

    if (ingrs.length != 0){
      for (var i = 0; i < ingrs.length; i++) { 
        if (forConcat){
          filterString += "&";
        }
        filterString += "ingredients[]=" + ingrs.at(i).value;
        if (i != category.length-1){
          filterString += "&";
        }
      } 
      forConcat = 1;
    }
    console.log(filterString);
    this.recipeService.refreshRecipes(filterString).subscribe((recipes : RecipeModel[]) => {
      this.recipes = recipes;
    });
  }
}