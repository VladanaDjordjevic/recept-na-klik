export interface User {
    _id: string;
    username: string;
    password: string;
    email:string;
    firstName:string;
    lastName: string;
    favorites: string[];
    createdAt:string[];
    updates: Map<string,string>
    photo: File;
}