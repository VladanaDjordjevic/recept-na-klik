import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LoginService } from './../services/login.service';
import { UserService } from '../services/user/user.service';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { RecipeService } from '../services/recipe/recipe.service';
import { RecipeModel } from '../services/recipe/recipe.model';
import { User } from '../services/user/user.model';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit, OnDestroy {

  public recipes: RecipeModel[] = [];
  public recipe: RecipeModel;
  public user: User;
  public usersCommenting: User[] = [];
  public imageContent;
  public loginUserImageContent;
  public authorUserImageContent;
  public usersCommentingImageContent = [];
  public comments: string[] = [];
  public loginUserId: string;
  public loginUser: User;
  public commentObject = {"userId" : '',
                          "comment" : ''};
  public scoreObject = {"score" : 0};
  public favoriteObject = {"favorites": ''};
  private activeSubscriptions: Subscription[] = [];
  private rId: string;
  public commentForm: FormGroup;

  constructor(private recipeService: RecipeService, private route: ActivatedRoute,
              private userService: UserService, private domS: DomSanitizer,
              public loginService: LoginService, private cdr: ChangeDetectorRef,
              private formBuilder: FormBuilder) {

                this.commentForm = this.formBuilder.group({
                  comment: []
                });
  }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.rId = params.get('recipeId');
      const subR = this.recipeService.getRecipeById(this.rId)
      .subscribe((recipe: RecipeModel) => {
          this.recipe = recipe;

          try {
            let TYPED_ARRAY = new Uint8Array(recipe.photo['data']['data']);
            let STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
            let base64String = btoa(STRING_CHAR);
            this.imageContent = this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);
          } catch (Error) {
            console.log(Error.message);
          }

          const subU = this.userService.getUserById(this.recipe.userId)
          .subscribe((user: User) => {
            this.user = user;

            console.log('OVAJ USER JE ', this.user.username);

            try {
              let TYPED_ARRAY = new Uint8Array(this.user.photo['data']['data']);
              let STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
              let base64String = btoa(STRING_CHAR);
              this.authorUserImageContent = this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);
            } catch(Error) {
              console.log(Error.message);
            }
          });

          this.activeSubscriptions.push(subU);

          for (let commentObject of this.recipe.comments) {
            const subUs = this.userService.getUserById(commentObject['userId'])
            .subscribe((user: User) => {
             this.usersCommenting.push(user);
             console.log('User koji komentarise je ', user);
             this.comments.push(commentObject['comment']);
             try {
              let TYPED_ARRAY = new Uint8Array(user.photo['data']['data']);
              let STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
              let base64String = btoa(STRING_CHAR);
              this.usersCommentingImageContent.push(this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String));
            } catch(Error) {
              console.log(Error.message);
            }
           });

           this.activeSubscriptions.push(subUs);

          }

          this.loginUserId = this.loginService.getLoggedInUserId();

          const subLU = this.userService.getUserById(this.loginUserId)
          .subscribe((user: User) => {
              this.loginUser = user;
              try {
                let TYPED_ARRAY = new Uint8Array(this.loginUser.photo['data']['data']);
                let STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
                let base64String = btoa(STRING_CHAR);
                this.loginUserImageContent = this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);
              } catch(Error) {
                console.log(Error.message);
              }
          });

          this.activeSubscriptions.push(subLU);

        });

        this.activeSubscriptions.push(subR);

    });

  }

  public refreshRecipe() {
    this.recipeService.getRecipeById(this.rId)
    .subscribe((recipe: RecipeModel) => {
      this.recipe = recipe;

      console.log("1. Proveravamo prisutnost komentara ", this.recipe.comments);

      for (let commentObject of this.recipe.comments) {
        const subUs = this.userService.getUserById(commentObject['userId'])
        .subscribe((user: User) => {
         this.usersCommenting.push(user);
         console.log('User koji komentarise je ', user);
         try {
          let TYPED_ARRAY = new Uint8Array(user.photo['data']['data']);
          let STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
          let base64String = btoa(STRING_CHAR);
          this.usersCommentingImageContent.push(this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String));
        } catch(Error) {
          console.log(Error.message);
        }
       });
      }
    });
  }

  public addComment(comm: string): void {

    this.commentObject['userId'] = this.loginUserId;
    this.commentObject['comment'] = comm;

    this.recipeService.giveScoreOrCommentToRecipe(this.commentObject, this.recipe._id, this.loginUserId, 'comment').subscribe();

    /*this.refreshRecipe(); */
  }

  public addScore(score) {

    if (this.loginUserId !== undefined) {
      if (this.recipe.scores["users"].indexOf(this.loginUserId) === -1) {
        this.scoreObject['score'] = Number(score);
        const subComm = this.recipeService.giveScoreOrCommentToRecipe(this.scoreObject, this.recipe._id, this.loginUserId, 'score')
        .subscribe();
        this.activeSubscriptions.push(subComm);

      } else {
        window.alert('Već ste ocenili recept');
      }
    } else {
      window.alert('Morate se ulogovati da biste ocenili recept');
    }

    this.cdr.markForCheck();
    this.cdr.detectChanges();
  }

  public addToFavorites() {
    if (this.loginUserId !== undefined) {
      this.userService.getFavoritesForUser(this.loginUserId)
      .subscribe((recipeIds: string[]) => {
        if (recipeIds.indexOf(this.recipe._id) === -1) {
          this.favoriteObject['favorites'] = this.recipe._id;
          this.userService.updateUser(this.favoriteObject, this.loginUserId).subscribe();
          window.alert('Recept je uspešno dodat u omiljene');
        } else {
          window.alert('Recept se vec nalazi u Vašim omiljenim');
        }
      });
    } else {
      window.alert('Morate se ulogovati da biste dodali recept u omiljene');
    }
  }

  public submitForm(data) {
    console.log(data);
    if (data['comment'] !== '' && data['comment'] !== null && this.loginUserId !== undefined) {
      this.addComment(data['comment']);
      this.commentForm.reset();
      window.alert('Uspešno ste komentarisali');

    } else if (data['comment'] === '' && data['comment'] === null) {
      window.alert("Komentar ne može biti prazan");
    } else {
      window.alert('Morate se ulogovati da biste mogli da komentarišete');
    }

  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

}
