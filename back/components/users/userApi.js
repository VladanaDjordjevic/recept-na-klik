const express = require('express');

const router = express.Router();

const controller = require('./usersController');

var multer  =   require('multer');
const path = require('path')

var storage =   multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../user_photos/'));
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.jpg');
  }
});
var upload = multer({ storage : storage}).single('photo');

router.get('/', controller.getUsers);
router.get('/:userId', controller.getByUserId);
router.get('/:userId/favorites', controller.getFavoritesByUserId);
router.delete('/:userId/favorites/:recipeId', controller.deleteFavorite);
router.post('/', upload, controller.createUser);
router.put('/:userId', upload, controller.updateByUserId);
router.delete('/:userId', controller.deleteByUserId);

module.exports = router;